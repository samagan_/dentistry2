﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DentistyServiceMigration.Migrations
{
    public partial class ErrorsOcurred : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Organizations_PatietId",
                table: "Patients");

            migrationBuilder.RenameColumn(
                name: "PatietId",
                table: "Patients",
                newName: "OrganizationId");

            migrationBuilder.RenameIndex(
                name: "IX_Patients_PatietId",
                table: "Patients",
                newName: "IX_Patients_OrganizationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Organizations_OrganizationId",
                table: "Patients",
                column: "OrganizationId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Organizations_OrganizationId",
                table: "Patients");

            migrationBuilder.RenameColumn(
                name: "OrganizationId",
                table: "Patients",
                newName: "PatietId");

            migrationBuilder.RenameIndex(
                name: "IX_Patients_OrganizationId",
                table: "Patients",
                newName: "IX_Patients_PatietId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Organizations_PatietId",
                table: "Patients",
                column: "PatietId",
                principalTable: "Organizations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
