﻿using DentistyServiceMigration.Models;
using System;
using System.Linq;

namespace DentistyServiceMigration
{
    class Program
    {
        static void Main(string[] args)
        {
            using (DSContext db = new DSContext())
            {
                Seed(db);
            }
        }

        public static void Seed(DSContext db)
        {
            if (!db.Patients.Any())
            {
                var organization = new Organization()
                {
                    Name = "Samagan Organization",
                    Address = "Bishkek",
                    Supervisor = "Samagan"
                };
                db.Organizations.Add(organization);
                db.SaveChanges();

                var patient = new Patient()
                {
                    FirstName = "Samagan",
                    LastName = "Karybekov",
                    Patronymic = "Kubanychbekovich",
                    DateOfBirth = new DateTime(2005, 02, 26),
                    CreatedAt = DateTime.Now,
                    Gender = true,
                    OrganizationId = organization.Id
                };
                db.Patients.Add(patient);
                db.SaveChanges();

                var visit = new Visit()
                {
                    VisitType = Enums.VisitType.FirstVisit,
                    VisitDate = DateTime.Now,
                    VisitStatus = Enums.VisitStatus.Created,
                    Price = 15000m,
                    PatietId = patient.Id
                };
                db.Visits.Add(visit);
                db.SaveChanges();

                var user = new User()
                {
                    Login = "Samagan.Karybekov@mail.com",
                    FIO = "Samagan Karybekov Kubanychbekovich",
                    Password = "samagan.2005",
                    blockedOrNot = Enums.BlockedOrNot.noBlocked,
                    CreateAccAt = DateTime.Now,
                    OrganizationId = organization.Id
                };
                db.Users.Add(user);
                db.SaveChanges();

                var teeth = new Teeth()
                {
                    JawType = Enums.JawType.Lower,
                    JawSide = Enums.JawSide.Left,
                    ToothNumber = 4
                };
                db.Teeths.Add(teeth);
                db.SaveChanges();

                var dentalCard = new DentalCard()
                {
                    Information = "All Great",
                    Comment = "Aibek is BEST reviewer :)",
                    TheethId = teeth.Id,
                    VisitId = visit.Id
                };
                db.DentalCards.Add(dentalCard);
                db.SaveChanges();
            }
        }
    }
}
