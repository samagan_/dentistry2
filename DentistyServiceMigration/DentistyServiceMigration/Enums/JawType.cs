﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistyServiceMigration.Enums
{
    public enum JawType
    {
        Upper = 1,
        Lower = 2
    }
}
