﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistyServiceMigration.Enums
{
    public enum JawSide
    {
        Left = 1,
        Right = 2
    }
}
