﻿using DentistyServiceMigration.Enums;
using DentistyServiceMigration.interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistyServiceMigration.Models
{
    public class Visit : IEntity<int>
    {
        public int Id { get; set; }
        public VisitType VisitType { get; set; }
        public DateTime VisitDate { get; set; }
        public VisitStatus VisitStatus { get; set; }
        public Nullable<decimal> Price { get; set; }
        [ForeignKey("Patient")]
        public int PatietId { get; set; }
        public virtual Patient Patient { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
