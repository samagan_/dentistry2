﻿using DentistyServiceMigration.Enums;
using DentistyServiceMigration.interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistyServiceMigration.Models
{
    public class User : IEntity<int>
    {
        public int Id { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        public string FIO { get; set; }
        [Required]
        public string Password { get; set; }
        public BlockedOrNot blockedOrNot { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreateAccAt{ get; set; }

        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
