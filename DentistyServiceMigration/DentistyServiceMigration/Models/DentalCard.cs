﻿using DentistyServiceMigration.interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistyServiceMigration.Models
{
    public class DentalCard : IEntity<int>
    {
        public int Id{ get; set; }

        [Required]
        public string Information{ get; set; }
        public string Comment { get; set; }

        [ForeignKey("Visit")]
        public int VisitId { get; set; }
        public virtual Visit Visit { get; set; }

        [ForeignKey("Teeth")]
        public int TheethId { get; set; }
        public virtual Teeth Teeth  { get; set; }
    }
}
