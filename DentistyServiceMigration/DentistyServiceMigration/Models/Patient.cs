﻿using DentistyServiceMigration.interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistyServiceMigration.Models
{
    public class Patient : IEntity<int>
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string FirstName{ get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        public bool Gender { get; set; }
        public DateTime CreatedAt { get; set; }

        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
