﻿using DentistyServiceMigration.Enums;
using DentistyServiceMigration.interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistyServiceMigration.Models
{
    public class Teeth : IEntity<int>
    {
        public int Id { get; set; }
        
        [Required]
        public JawType JawType { get; set; }

        [Required]
        public JawSide JawSide { get; set; }
        public int ToothNumber { get; set; }

        [ForeignKey("DentalCard")]
        public int DentalCardId { get; set; }
        public virtual DentalCard DentalCard { get; set; }
    }
}
